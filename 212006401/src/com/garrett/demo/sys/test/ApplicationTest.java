package com.garrett.demo.sys.test;

import cn.smalltool.captcha.generator.RandomGenerator;
import cn.smalltool.core.utils.IdcardUtil;
import cn.smalltool.core.utils.PhoneUtil;
import cn.smalltool.core.utils.StrUtil;
import com.garrett.demo.sys.dao.UserDao;
import com.garrett.demo.sys.dao.UserDataBaseDemo;
import com.garrett.demo.sys.entity.Account;
import com.garrett.demo.sys.entity.SimsStudent;
import com.garrett.demo.sys.entity.UserInfo;
import com.garrett.demo.sys.service.AccountService;
import com.garrett.demo.sys.service.AccountServiceImp;
import com.garrett.demo.sys.service.OtherLogicMethod;
import com.sun.javaws.IconUtil;

import java.util.Objects;
import java.util.Scanner;

public class ApplicationTest {

    public static void commandTemplate(){
        System.out.println("===================================");
        System.out.println("|                                 |");
        System.out.println("|            极简登录系统           ｜");
        System.out.println("|                                 |");
        System.out.println("===================================");
        System.out.println("1.学号登录\t\t\t5.删除用户信息");
        System.out.println("2.新生注册\t\t\t6.修改账户信息");
        System.out.println("3.找回密码\t\t\t7.修改学生信息");
        System.out.println("4.查看所有账户信息\t\t0.退出");
    }

    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        AccountServiceImp as=new AccountServiceImp();

        while(true) {
            // 命令窗口启动模版
            commandTemplate();

            System.out.print("\n请输入程序指令:");
            int select = scanner.nextInt();
            if (select == 0) break;
            switch (select) {
                case 1:
                    login(as);
                    break;
                case 2:
                    register(as);
                    break;
                case 3:
                    forget(as);
                    break;
                case 4:
                    seeAllAccounts(as);
                    break;
                case 5:
                    deleteAccount(as);
                    break;
                case 6:
                    modify(as);
                    break;
                case 7:

            }
        }

    }


    //查看所有账户信息
    static void seeAllAccounts(AccountServiceImp as){
        Account[] accounts=as.getAllAccounts();
        SimsStudent[] students=as.getAllStudents();
        System.out.println("当前所有的信息为:");
        for(int i=0;i<accounts.length && accounts[i]!=null;i++){
            System.out.println("编号:"+accounts[i].getId()+"\t账户名:"+accounts[i].getAccount());
            System.out.println("\t学生学号:"+students[i].getStudentId()+"\n\t学生姓名:"+students[i].getStudentName()
                +"\n\t学生身份证:"+students[i].getIdCardNo()+"\n\t学生电话:"+students[i].getMobilePhone());
        }
    }

    //删除用户信息
    static void deleteAccount(AccountServiceImp as){
        Scanner scanner=new Scanner(System.in);
        System.out.print("请输入要删除的账户编号:");
        String id = scanner.nextLine();
        if(as.getStuIdById(id)==null){
            System.out.println("输入的编号无效！");
            return ;
        }
        as.deleteAccount(id);
        if(as.getStuIdById(id)==null){
            System.out.println("删除成功！");
            seeAllAccounts(as);
        }
    }


    //修改账户信息
    static void modify(AccountServiceImp as){
        Scanner scanner=new Scanner(System.in);
        System.out.print("请输入你要修改的账户:");
        String account = scanner.nextLine();
        Account a=as.getUserByAccount(account);
        if(a==null){
            System.out.println("输入的账户名有误！");
            return ;
        }
        System.out.print("请输入你要修改的密码:");
        String password = scanner.nextLine();
        a.setPassword(password);
        as.modify(a);
        System.out.println("修改成功！");
    }


    //登录模块
    static void login(AccountServiceImp accountService){
        Scanner scanner=new Scanner(System.in);

        System.out.print("用户名:");
        String account = scanner.nextLine();
        System.out.print("密码：");
        String password = scanner.nextLine();

        // 验证码功能
        codeCheck();

        SimsStudent userInfo = accountService.checkLogin(account, password);

        // 登录结果消息提示
        loginResMessage(userInfo);
    }

    static void register(AccountServiceImp as){
        String stuId,name,idCard,pw1,pw2,phone,sex;
        Scanner scanner=new Scanner(System.in);
        SimsStudent ss;
        while(true){
            while(true){
                System.out.print("请输入学号:");
                stuId=scanner.nextLine();
                if(!as.verifyStuId(stuId))
                    System.out.println("学号输入错误！");
                else if(as.checkStuId(stuId))
                    System.out.println("学号已存在！");
                else break;
            }
            System.out.print("请输入姓名:");
            name = scanner.nextLine();

            while(true){
                System.out.print("请输入密码:");
                pw1=scanner.nextLine();
                System.out.print("请再次确认密码:");
                pw2=scanner.nextLine();
                if(StrUtil.isBlank(pw1))
                    System.out.println("密码不能为空！");
                else if(!as.checkPasswordLength(pw1))
                    System.out.println("密码长度为6到16位！");
                else if(!as.verifyPassword(pw1,pw2))
                    System.out.println("两次密码不一致！");
                else
                    break;
            }

            // 验证码功能
            codeCheck();

            while(true){
                System.out.print("请输入身份证号:");
                idCard=scanner.nextLine();
                if(!as.verifyIdCard(idCard))
                    System.out.println("身份证号输入错误！");
                else
                    break;
            }

            while(true){
                System.out.print("请输入手机号:");
                phone=scanner.nextLine();
                if(!PhoneUtil.isPhone(phone))
                    System.out.println("手机号输入错误！");
                else
                    break;
            }

            while(true){
                System.out.print("请输入性别:");
                sex=scanner.nextLine();
                if(!Objects.equals(sex, "男") && !Objects.equals(sex, "女"))
                    System.out.println("输入的性别有误！");
                else if(IdcardUtil.getGenderByIdCard(idCard)!=(Objects.equals(sex, "男") ? 1:0))
                    System.out.println("输入的身份证与性别不匹配！");
                else
                    break;
            }

            System.out.println("您的密码强度为："+as.checkPasswordStrength(pw1));
            System.out.println("确认注册:1\t重新注册:2\t返回登录模块:0");
            System.out.print("请输入程序指令:");
            int select=scanner.nextInt();
            scanner.nextLine();
            if(select==1)
                break;
            else if(select==2)
                continue;
            else
                return;
        }
        System.out.println();
        ss=new SimsStudent();
        ss.setStudentName(name);
        ss.setStudentId(stuId);
        ss.setMobilePhone(phone);
        ss.setIdCardNo(idCard);
        ss.setGender(sex);
        Account account=new Account();
        account.setPassword(pw1);
        as.createAccount(ss,account);
        System.out.println("注册成功！");
    }

    public static void forget(AccountServiceImp as){
        Scanner scanner=new Scanner(System.in);
        System.out.print("请输入要重置密码的用户名:");
        String name=scanner.nextLine();
        codeCheck();
        System.out.print("请输入您的身份证号:");
        String idCard=scanner.nextLine();
        if(as.checkAccountByIdCard(name,idCard)){
            System.out.print("请输入新的密码:");
            String pw1=scanner.nextLine();
            System.out.print("请再次确认密码:");
            String pw2=scanner.nextLine();
            if(StrUtil.isBlank(pw1))
                System.out.println("密码不能为空！");
            else if(!as.checkPasswordLength(pw1))
                System.out.println("密码长度为6到16位！");
            else if(!as.verifyPassword(pw1,pw2))
                System.out.println("两次密码不一致！");
            else {
                as.changePassword(name,pw1);
                System.out.println("密码已重置！");
                return;
            }
        }
        System.out.println("密码重置失败！");
    }


    static void codeCheck(){
        Scanner sc = new Scanner(System.in);
        while (true) {
            //提前吞掉空格，防止后面吞掉
            //  sc.nextLine();

            // 长度为4的随机码生成器
            RandomGenerator randomGenerator = new RandomGenerator(4);
            String randomStr = randomGenerator.generate();
            System.out.println("\n请输入验证码，以区分您不是机器人");
            System.out.println("=========================");
            System.out.println("|  验证码  |    " + randomStr + "     |");
            System.out.println("=========================");


            // 获取输入的验证码
            String code = sc.next().trim();
            if (randomGenerator.verify(randomStr,code)) {
                return;
            }else{
                System.out.println("验证码错误！");
                System.out.println("正在重置验证码...");
            }
        }
    }

    static void loginResMessage(SimsStudent userInfo){
        if (userInfo != null) {
            System.out.println("登录成功!!!");
            System.out.println("欢迎登录系统，" + userInfo.getStudentName());
        }else{
            System.out.println("登录失败!!!");
            System.out.println("提示：账号或密码错误.");
        }
    }

}
