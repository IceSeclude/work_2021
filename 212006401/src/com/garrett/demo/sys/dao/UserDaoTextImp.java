package com.garrett.demo.sys.dao;

import com.garrett.demo.sys.entity.Account;
import com.garrett.demo.sys.entity.SimsStudent;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class UserDaoTextImp implements UserDao{

    /**
     * 只模拟了学生信息的数据源文件
     */
    public static final String PATH = "src/main/resources/sims_student.txt";

    private File fileSource;

    public UserDaoTextImp() {
        // 默认构造函数数据源
        fileSource = new File(PATH);
    }

    /**
     * 由构造函数指定数据源
     * @param filePath
     */
    public UserDaoTextImp(String filePath) {
        String path = filePath == null ? PATH : filePath;
        fileSource = new File(path);
    }

    /**
     * 根据用户编号获取用户信息
     *
     * @param id 用户编号
     * @return 用户信息
     */
    @Override
    public SimsStudent getStudentById(String id) {
        InputStreamReader read = null;
        try {
            if(fileSource.isFile() && fileSource.exists()){ //判断文件是否存在
                read = new InputStreamReader(
                        new FileInputStream(fileSource), StandardCharsets.UTF_8);//考虑到编码格式
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt;
                while((lineTxt = bufferedReader.readLine()) != null) {
                    SimsStudent stu = recordFormat(lineTxt);
                    if(stu.getStudentId().equals(id)) return stu;
                }
            }else{
                System.out.println("找不到指定的文件");
            }
        } catch (Exception e) {
            System.out.println("读取文件内容出错");
            e.printStackTrace();
        } finally {
            if (read != null) {
                try {
                    read.close();
                } catch (IOException e) {
                    System.out.println("资源关闭异常");
                }
            }
        }
        return null;
    }

    /**
     * 单行数据记录处理
     * @param row 行记录
     * @return 学生信息
     */
    SimsStudent recordFormat(String row){
        SimsStudent stu = null;

        int beginIndex = row.indexOf("{") + 1;
        int endIndex = row.indexOf("}");
        String content = row.substring(beginIndex,endIndex);
        String[] split = content.split(",");
        // 知识点
        String[] fields = Arrays.stream(split).map(String::trim).toArray(String[]::new);
        if(fields.length > 0){
            stu = new SimsStudent();
            stu.setStudentId(fields[0]);
            stu.setStudentName(fields[1]);
            stu.setEngName(fields[2]);
            stu.setIdCardNo(fields[3]);
            stu.setMobilePhone(fields[4]);
        }
        return stu;
    }

    /**
     * 单行数据记录处理
     * @param row 行记录
     * @return 账户信息
     *
     * <p>假定数据源中 内容为 {1, 212006677, admin} 格式</p>
     * <p>以下方法是针对该内容实现的逻辑</p>
     */
    private Account recordFormatter(String row){
        Account ac = null;
        int beginIndex = row.indexOf("{") + 1;
        int endIndex = row.indexOf("}");
        String content = row.substring(beginIndex,endIndex);
        String[] split = content.split(",");
        String[] fields = Arrays.stream(split).map(String::trim).toArray(String[]::new);
        if(fields.length > 0){
            ac = new Account();
            ac.setId(fields[0]);
            ac.setAccount(fields[1]);
            ac.setPassword(fields[2]);
        }
        return ac;
    }

    /**
     * 根据登录账号获取用户信息
     *
     * @param account 账户名
     * @return 用户信息
     */
    @Override
    public Account getUserByAccount(String account) {
        /**
         * 这里 fileSource 指向由 构造方法 约定
         */
        InputStreamReader read = null;
        try {
            if(fileSource.isFile() && fileSource.exists()){ //判断文件是否存在
                read = new InputStreamReader(
                        new FileInputStream(fileSource), StandardCharsets.UTF_8);//考虑到编码格式
                BufferedReader bufferedReader = new BufferedReader(read);
                String lineTxt;
                while((lineTxt = bufferedReader.readLine()) != null) {
                    // 知识点
                    Account ac = recordFormatter(lineTxt);
                    if(ac.getAccount().equals(account)) return ac;
                }
            }else{
                System.out.println("找不到指定的文件");
            }
        } catch (Exception e) {
            System.out.println("读取文件内容出错");
            e.printStackTrace();
        } finally {
            if (read != null) {
                try {
                    read.close();
                } catch (IOException e) {
                    System.out.println("资源关闭异常");
                }
            }
        }
        return null;
    }

    /**
     * 注册账户信息
     *
     * @param account 账户
     * @return 1-成功 0-失败
     */
    @Override
    public int addAccount(Account account) {
        // TODO 完善代码.... 保证程序功能正常
        return 0;
    }

    /**
     * 删除账户
     *
     * @param id 主键id - 不是学号
     * @return 1-成功 0-失败
     */
    @Override
    public int delAccountById(String id) {
        // TODO 实训二作业

        return 0;
    }

    /**
     * 修改账户信息
     *
     * @param account 主键id不可改变，根据主键id判断修改。
     *                <p>例子：{id:23,account:212006677,password:admin}</p>
     *                <p>根据id判断数组中是否存在对应id值，如存在则可以修改学号和密码，否则无效修改</p>
     * @return 1-成功 0-失败
     */
    @Override
    public int modifyAccount(Account account) {
        // TODO 实训二作业

        return 0;
    }

    /**
     * 添加学生信息
     *
     * @param student 学生信息
     * @return
     */
    @Override
    public int createUser(SimsStudent student) {

        // TODO 完善代码.... 保证程序功能正常
        return 0;
    }

    /**
     * 删除学生信息
     *
     * @param stuId 学号
     * @return
     */
    @Override
    public int delStudentInfo(String stuId) {
        // TODO 实训二作业

        return 0;
    }

    /**
     * 修改学生信息
     *
     * <p>studentId(学号)不可改变，根据主键studentId判断修改</p>
     * <p>例子：{studentId:212006677,studentName:garrett,email:null}</p>
     * <p>根据studentId判断数组中是否存在对应学号，如存在则可以修改除学号以外的对应属性值，否则无效修改</p>
     *
     * @param student 学生信息
     * @return 1-成功 0-失败
     */
    @Override
    public int modifyStudentInfo(SimsStudent student) {
        // TODO 实训二作业

        return 0;
    }

    /**
     * 获取所有学生信息
     *
     * @return 学生信息列表
     */
    @Override
    public SimsStudent[] getAllStudent() {

        // TODO 完善代码.... 保证程序功能正常
        return new SimsStudent[0];
    }
}
