package com.garrett.demo.sys.dao;

import com.garrett.demo.sys.entity.Account;
import com.garrett.demo.sys.entity.SimsStudent;

public class UserDataBaseDemo implements UserDao{
    private SimsStudent[] students=new SimsStudent[3];
    private Account[] accounts=new Account[3];


    {
        students[0]=new SimsStudent("212006401","刘浩","123456789123456789",
        "12345678912","男","1234567891@qq.com");
        students[1]=new SimsStudent("admin","admin","null",
                "null","null","null");
        accounts[0]=new Account("1","212006401","wangpeiming");
        accounts[1]=new Account("2","admin","admin");
    }


    //通过学号获取学生信息
    public SimsStudent getStudentById(String stuId){
        for (SimsStudent student : students) {
            if (student.getStudentId().equals(stuId))
                return student;
        }
        return null;
    }


    //通过账户名获取用户信息
    public Account getUserByAccount(String account){
        for (Account value : accounts) {
            if (value.getAccount().equals(account))
                return value;
        }
        return null;
    }


    //通过编号获取账户名（学号）
    public String getStuIdById(String id){
        for(int i=0; i < accounts.length && accounts[i]!=null; i++ ){
            if(accounts[i].getId().equals(id)){
                return students[i].getStudentId();
            }
        }
        return null;
    }


    //添加账户
    public int addAccount(Account account){
        for(int i=0;i<accounts.length;i++)
            if(accounts[i]==null){
                accounts[i]=account;
                return 1;
            }
        return 0;
    }


    //创建用户（学生信息）
    public int createUser(SimsStudent userInfo){
        for(int i=0;i<students.length;i++)
            if(students[i]==null){
                students[i]=userInfo;
                return 1;
            }
        return 0;
    }


    //获取所有学生信息
    public SimsStudent[] getAllStudent(){
        return students;
    }


    //获取所有账户信息
    public Account[] getAccounts(){
        return accounts;
    }


    /**
     * 删除账户
     *
     * @param id 主键id - 不是学号
     * @return 1-成功 0-失败
     */
    //通过id删除用户信息（即编号）
    @Override
    public int delAccountById(String id) {
        // TODO 实训二作业
        for(int i=0; i < accounts.length && accounts[i]!=null; i++ ){
            if(accounts[i].getId().equals(id)){
                for(int j=i;j<accounts.length && accounts[j]!=null;j++){
                    accounts[j]=accounts[j+1];
                }
                if(accounts[accounts.length-1]!=null)accounts[accounts.length-1]=null;
                return 1;
            }
        }
        return 0;
    }


    /**
     * 修改账户信息
     *
     * @param account 主键id不可改变，根据主键id判断修改。
     *                <p>例子：{id:23,account:212006677,password:admin}</p>
     *                <p>根据id判断数组中是否存在对应id值，如存在则可以修改学号和密码，否则无效修改</p>
     * @return 1-成功 0-失败
     */
    //修改账户信息（需要验证编号是否存在）
    @Override
    public int modifyAccount(Account account) {
        // TODO 实训二作业
        for(int i=0; i<accounts.length && accounts[i]!=null;i++){
            if(accounts[i].getId().equals(account.getId())){
                accounts[i]=account;
                return 1;
            }
        }
        return 0;
    }


    /**
     * 删除学生信息
     *
     * @param stuId 学号
     * @return
     */
    //通过学生id删除学生信息
    @Override
    public int delStudentInfo(String stuId) {
        // TODO 实训二作业
        for(int i=0;i<students.length && students[i]!=null;i++){
            if(students[i].getStudentId().equals(stuId)){
                for(int j=i;j<students.length && students[j]!=null;j++){
                    students[j]=students[j+1];
                }
                if(students[students.length-1]!=null)students[students.length-1]=null;
                return 1;
            }
        }
        return 0;
    }


    /**
     * 修改学生信息
     *
     * <p>studentId(学号)不可改变，根据主键studentId判断修改</p>
     * <p>例子：{studentId:212006677,studentName:garrett,email:null}</p>
     * <p>根据studentId判断数组中是否存在对应学号，如存在则可以修改除学号以外的对应属性值，否则无效修改</p>
     *
     * @param student 学生信息
     * @return 1-成功 0-失败
     */
    //通过学生id修改学生信息
    @Override
    public int modifyStudentInfo(SimsStudent student) {
        // TODO 实训二作业
        for(int i=0;i<students.length && students[i]!=null;i++){
            if(students[i].getStudentId().equals(student.getStudentId())){
                students[i]=student;
                return 1;
            }
        }
        return 0;
    }

}
