package com.garrett.demo.sys.dao;

import com.garrett.demo.sys.entity.Account;
import com.garrett.demo.sys.entity.SimsStudent;

public interface UserDao {

    /**
     * 注册账户信息
     * @param account 账户
     * @return 1-成功插入一条数据 0-数据创建失败
     */
    int addAccount(Account account);

    /**
     * 删除账户
     * @param id 主键id - 不是学号
     * @return 1-成功 0-失败
     */
    int delAccountById(String id);

    /**
     * 修改账户信息
     * @param account 主键id不可改变，根据主键id判断修改。
     *                <p>例子：{id:23,account:212006677,password:admin}</p>
     *                <p>根据id判断数组中是否存在对应id值，如存在则可以修改学号和密码，否则无效修改</p>
     * @return 1-成功 0-失败
     */
    int modifyAccount(Account account);

    /**
     * 根据登录账号获取账户资源信息
     * @param account 账户名
     * @return 成功-账户信息 失败-null
     */
    Account getUserByAccount(String account);

    /**
     * 添加用户信息
     * @param userInfo 用户信息
     * @return 1-成功插入一条数据 0-数据创建失败
     */
    int createUser(SimsStudent userInfo);

    /**
     * 删除学生信息
     * @param stuId 学号
     * @return
     */
    int delStudentInfo(String stuId);

    /**
     * 修改学生信息
     *
     * <p>studentId(学号)不可改变，根据主键studentId判断修改</p>
     * <p>例子：{studentId:212006677,studentName:garrett,email:null}</p>
     * <p>根据studentId判断数组中是否存在对应学号，如存在则可以修改除学号以外的对应属性值，否则无效修改</p>
     *
     * @param student 学生信息
     * @return 1-成功 0-失败
     */
    int modifyStudentInfo(SimsStudent student);

    /**
     * 根据学号获取学生信息
     * @param stuId 学号
     * @return 成功-用户信息 失败-null
     */
    SimsStudent getStudentById(String stuId);

    /**
     * 获取所有学生信息
     * @return 学生信息列表
     */
    SimsStudent[] getAllStudent();

}
