package com.garrett.demo.sys.entity;

/**
 * 账号资源
 */
public class Account {

    /**
     * 编号
     */
    private String id;

    /**
     * 账户名
     */
    private String account;

    /**
     * 密码
     */
    private String password;

    public Account() {
    }

    public Account(String id, String account, String password) {
        this.id = id;
        this.account = account;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
