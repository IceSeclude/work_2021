package com.garrett.demo.sys.entity;

/**
 * 用户信息
 */
public class UserInfo {

    public UserInfo() {
    }

    public UserInfo(String id, String stuId, String realName) {
        this.id = id;
        this.stuId = stuId;
        this.realName = realName;
    }

    /**
     * 编号 - 唯一识别码
     */
    private String id;

    /**
     * 学号
     */
    private String stuId;

    /**
     * 用户真实姓名
     */
    private String realName;

    /**
     * 性别
     */
    private String gender;

    /**
     * 手机号
     */
    private String phoneNumber;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStuId() {
        return stuId;
    }

    public void setStuId(String stuId) {
        this.stuId = stuId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }
}
