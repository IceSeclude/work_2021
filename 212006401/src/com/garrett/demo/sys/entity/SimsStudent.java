package com.garrett.demo.sys.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @author : http://www.chiner.pro
 * @date : 2021-11-1
 * @desc : 学生
 */
public class SimsStudent implements Serializable{
    /** 学生ID */
    private String studentId ;
    /** 学生姓名 */
    private String studentName ;
    /** 英文名 */
    private String engName ;
    /** 身份证号 */
    private String idCardNo ;
    /** 手机号 */
    private String mobilePhone ;
    /** 性别 */
    private String gender ;
    /** 身高 */
    private Integer height ;
    /** 体重 */
    private Integer weight ;
    /** 民族 */
    private String nation ;
    /** 政治面貌 */
    private String political ;
    /** 籍贯（省） */
    private String domicilePlaceProvince ;
    /** 籍贯（市） */
    private String domicilePlaceCity ;
    /** 户籍地址 */
    private String domicilePlaceAddress ;
    /** 爱好 */
    private String hobby ;
    /** 简要介绍 */
    private String intro ;
    /** 居住地址 */
    private String presentAddress ;
    /** 电子邮件 */
    private String email ;
    /** 入学日期 */
    private Date entryDate ;

    public SimsStudent() {
    }

    public SimsStudent(String studentId, String studentName, String idCardNo, String mobilePhone, String gender, String email) {
        this.studentId = studentId;
        this.studentName = studentName;
        this.idCardNo = idCardNo;
        this.mobilePhone = mobilePhone;
        this.gender = gender;
        this.email = email;
    }

    /** 学生ID */
    public String getStudentId(){
        return this.studentId;
    }
    /** 学生ID */
    public void setStudentId(String studentId){
        this.studentId=studentId;
    }
    /** 学生姓名 */
    public String getStudentName(){
        return this.studentName;
    }
    /** 学生姓名 */
    public void setStudentName(String studentName){
        this.studentName=studentName;
    }
    /** 英文名 */
    public String getEngName(){
        return this.engName;
    }
    /** 英文名 */
    public void setEngName(String engName){
        this.engName=engName;
    }
    /** 身份证号 */
    public String getIdCardNo(){
        return this.idCardNo;
    }
    /** 身份证号 */
    public void setIdCardNo(String idCardNo){
        this.idCardNo=idCardNo;
    }
    /** 手机号 */
    public String getMobilePhone(){
        return this.mobilePhone;
    }
    /** 手机号 */
    public void setMobilePhone(String mobilePhone){
        this.mobilePhone=mobilePhone;
    }
    /** 性别 */
    public String getGender(){
        return this.gender;
    }
    /** 性别 */
    public void setGender(String gender){
        this.gender=gender;
    }

    /** 身高 */
    public Integer getHeight(){
        return this.height;
    }
    /** 身高 */
    public void setHeight(Integer height){
        this.height=height;
    }
    /** 体重 */
    public Integer getWeight(){
        return this.weight;
    }
    /** 体重 */
    public void setWeight(Integer weight){
        this.weight=weight;
    }
    /** 名族 */
    public String getNation(){
        return this.nation;
    }
    /** 名族 */
    public void setNation(String nation){
        this.nation=nation;
    }
    /** 政治面貌 */
    public String getPolitical(){
        return this.political;
    }
    /** 政治面貌 */
    public void setPolitical(String political){
        this.political=political;
    }
    /** 籍贯（省） */
    public String getDomicilePlaceProvince(){
        return this.domicilePlaceProvince;
    }
    /** 籍贯（省） */
    public void setDomicilePlaceProvince(String domicilePlaceProvince){
        this.domicilePlaceProvince=domicilePlaceProvince;
    }
    /** 籍贯（市） */
    public String getDomicilePlaceCity(){
        return this.domicilePlaceCity;
    }
    /** 籍贯（市） */
    public void setDomicilePlaceCity(String domicilePlaceCity){
        this.domicilePlaceCity=domicilePlaceCity;
    }
    /** 户籍地址 */
    public String getDomicilePlaceAddress(){
        return this.domicilePlaceAddress;
    }
    /** 户籍地址 */
    public void setDomicilePlaceAddress(String domicilePlaceAddress){
        this.domicilePlaceAddress=domicilePlaceAddress;
    }
    /** 爱好 */
    public String getHobby(){
        return this.hobby;
    }
    /** 爱好 */
    public void setHobby(String hobby){
        this.hobby=hobby;
    }
    /** 简要介绍 */
    public String getIntro(){
        return this.intro;
    }
    /** 简要介绍 */
    public void setIntro(String intro){
        this.intro=intro;
    }
    /** 居住地址 */
    public String getPresentAddress(){
        return this.presentAddress;
    }
    /** 居住地址 */
    public void setPresentAddress(String presentAddress){
        this.presentAddress=presentAddress;
    }
    /** 电子邮件 */
    public String getEmail(){
        return this.email;
    }
    /** 电子邮件 */
    public void setEmail(String email){
        this.email=email;
    }
    /** 入学日期 */
    public Date getEntryDate(){
        return this.entryDate;
    }
    /** 入学日期 */
    public void setEntryDate(Date entryDate){
        this.entryDate=entryDate;
    }


    @Override
    public String toString() {
        return "{" +
                ", " + studentId +
                ", " + studentName +
                ", " + engName +
                ", " + idCardNo +
                ", " + mobilePhone +
                ", " + gender +
                ", " + height +
                ", " + weight +
                ", " + nation +
                ", " + political +
                ", " + domicilePlaceProvince +
                ", " + domicilePlaceCity +
                ", " + domicilePlaceAddress +
                ", " + hobby +
                ", " + intro +
                ", " + presentAddress +
                ", " + email +
                ", " + entryDate +
                '}';
    }
}