package com.garrett.demo.sys.service;


import cn.smalltool.core.lang.Validator;
import cn.smalltool.core.utils.IdcardUtil;
import cn.smalltool.core.utils.StrUtil;
import com.garrett.demo.sys.dao.UserDataBaseDemo;
import com.garrett.demo.sys.entity.Account;
import com.garrett.demo.sys.entity.SimsStudent;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class AccountServiceImp extends AbstractAccountService {
    private UserDataBaseDemo uDBD=new UserDataBaseDemo();

    @Override
    public SimsStudent checkLogin(String account, String password) {

            if(uDBD.getUserByAccount(account)!=null &&
                    uDBD.getUserByAccount(account).getPassword().equals(password))
                return uDBD.getStudentById(account);
        return null;
    }

    @Override
    public void createAccount(Object... obj) {
        Account account=(Account) obj[1];
        SimsStudent ss=(SimsStudent)obj[0];
        uDBD.createUser(ss);
        account.setAccount(ss.getStudentId());
        uDBD.addAccount(account);
    }

    @Override
    public boolean verifyPassword(String pw1, String pw2) {
        return pw1.equals(pw2);
    }

    public boolean checkPasswordLength(String pw){
        return pw.length()>=6 && pw.length()<=16;
    }

    @Override
    public boolean verifyIdCard(String idCard) {
        return IdcardUtil.isValidCard(idCard);
    }

    @Override
    public boolean verifyStuId(String id) {
        if(StrUtil.isNotBlank(id)){
            try {
                Integer.valueOf(id);
                return true;
            }catch (Exception e){
                return false;
            }
        }
        return false;
    }

    //注册判断学号是否已存在
    public boolean checkStuId(String stuId){
        SimsStudent[] ss= uDBD.getAllStudent();
        for(int i=0;i<ss.length && ss[i]!=null;i++){
            if(ss[i].getStudentId().equals(stuId))
                return true;
        }
        return false;
    }

    //检查密码强度
    public String checkPasswordStrength(String password){
        int strength=0;
        int digital=0,lwr=0,upr=0,other=0;
        //统计密码中类型数量
        for(int i=0;i<password.length();i++){
            char a=password.charAt(i);
            if(a<='9' && a>='0')digital++;
            else if(a<='z' && a>='a')lwr++;
            else if(a<='Z' && a>='A')upr++;
            else other++;
        }
        if(digital>0)strength++;
        if(lwr>0)strength++;
        if(upr>0)strength++;
        if(other>0)strength++;

        String message;
        switch (strength){
            case 1: return "低";
            case 2: return "中";
            case 3: return "高";
            case 4: return "超高";
        }
        return null;
    }

    //忘记密码后检查账户名和身份证是否符合要求
    public boolean checkAccountByIdCard(String account,String idCard){
        return uDBD.getStudentById(account).getIdCardNo().equals(idCard);
    }


    //修改密码
    public void changePassword(String account,String password){
        uDBD.getUserByAccount(account).setPassword(password);
    }


    /**
     * 获取所有账户信息
     *
     * @return 账户列表
     */
    //获取所有账户信息
    public Account[] getAllAccounts() {
        return uDBD.getAccounts();
    }


    /**
     * 获取所有学生信息
     *
     * @return 学生信息列表
     */
    //获取所有学生信息
    @Override
    public SimsStudent[] getAllStudents() {
        return uDBD.getAllStudent();
    }


    /**
     * 根据学号获取学生信息
     *
     * @return
     */
    //根据学号获取学生信息
    @Override
    public SimsStudent getStudentById(String studentId) {
        return uDBD.getStudentById(studentId);
    }


    /**
     * 修改数据 - 用于学生信息或账户信息修改
     * <p> Tips: objs[0] instanceof Account </p>
     *
     * @param objs 对象数组
     */
    //用于学生信息或账户信息修改
    @Override
    public void modify(Object... objs) {
        // TODO 实训二作业
        if(objs[0] instanceof Account){
            uDBD.modifyAccount((Account) objs[0]);
        }
        else if(objs[0] instanceof SimsStudent){
            uDBD.modifyStudentInfo((SimsStudent) objs[0]);
        }
    }


    /**
     * 删除账户信息
     *
     * <p> 通过主键，以及学号间的绑定关系，同步删除数据空间中的account，和student</p>
     *
     * @param id 账户主键id
     */
    //通过编号删除账户和学生信息
    @Override
    public void deleteAccount(String id) {
        String si=getStuIdById(id);
        if(!si.isEmpty()){
            uDBD.delAccountById(id);
            uDBD.delStudentInfo(si);
        }
    }

    //通过编号获取账户名（学号）
    public String getStuIdById(String id){
        return uDBD.getStuIdById(id);
    }

    //通过用户名获取用户信息
    public Account getUserByAccount(String account) {
        return uDBD.getUserByAccount(account);
    }
}
