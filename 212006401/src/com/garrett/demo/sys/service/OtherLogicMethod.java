package com.garrett.demo.sys.service;

public interface OtherLogicMethod {

    /**
     * 密码比对
     * @param pw1 密码1
     * @param pw2 密码2
     * @return true-密码一致 false-密码不匹配
     */
    boolean verifyPassword(String pw1,String pw2);

    /**
     * 校验身份证号码
     * @param idCard 身份证号码
     * @return true-验证成功 false-失败
     */
    boolean verifyIdCard(String idCard);

    /**
     * 校验学号
     * @param id 学号
     * @return true-验证成功 false-失败
     */
    boolean verifyStuId(String id);

}
