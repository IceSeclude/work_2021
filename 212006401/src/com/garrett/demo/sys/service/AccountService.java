package com.garrett.demo.sys.service;

import com.garrett.demo.sys.entity.Account;
import com.garrett.demo.sys.entity.SimsStudent;

public interface AccountService {

    /**
     * 验证信息
     * @param account 账户名
     * @param password 密码
     * @return 用户信息
     */
    SimsStudent checkLogin(String account, String password);

    /**
     * 注册账户
     * @param obj 账户信息
     */
    void createAccount(Object... obj);

    /**
     * 获取所有账户信息
     * @return 账户列表
     */
    Account[] getAllAccounts();

    /**
     * 获取所有学生信息
     * @return 学生信息列表
     */
    SimsStudent[] getAllStudents();

    /**
     * 根据学号获取学生信息
     * @param studentId 学号
     * @return
     */
    SimsStudent getStudentById(String studentId);

    /**
     * 修改数据 - 用于学生信息或账户信息修改
     * <p> Tips: objs[0] instanceof Account </p>
     * @param objs 对象数组
     */
    void modify(Object... objs);

    /**
     * 删除账户信息
     *
     * <p> 通过主键，以及学号间的绑定关系，同步删除数据空间中的account，和student</p>
     *
     * @param id 账户主键id
     */
    void deleteAccount(String id);
}
