package cn.smalltool.captcha.generator;

/**
 * 抽象生成器
 */
public abstract class AbstractGenerator implements cn.smalltool.captcha.generator.CodeGenerator {

    // 字典：基础字符串
    protected final String baseStr;
    // 生成的长度
    protected final int length;

    public AbstractGenerator(int count) {
        this("abcdefghijklmnopqrstuvwxyz0123456789", count);
    }

    public AbstractGenerator(String baseStr, int length) {
        this.baseStr = baseStr;
        this.length = length;
    }

    public int getLength() {
        return this.length;
    }
}
