package cn.smalltool.captcha.generator;

import cn.smalltool.core.utils.RandomUtil;
import cn.smalltool.core.utils.StrUtil;

/**
 * 随机验证码
 */
public class RandomGenerator extends AbstractGenerator{

    public RandomGenerator(int count) {
        super(count);
    }

    public RandomGenerator(String baseStr, int length) {
        super(baseStr, length);
    }

    /**
     * 生成器
     *
     * @return 验证码
     */
    @Override
    public String generate() {
        return RandomUtil.randomString(this.baseStr, this.length);
    }

    /**
     * 校验
     *
     * @param code 生成码
     * @param userInputCode 用户输入的验证码
     */
    @Override
    public boolean verify(String code, String userInputCode) {
        return StrUtil.isNotBlank(userInputCode) && StrUtil.equalsIgnoreCase(code, userInputCode);
    }
}
