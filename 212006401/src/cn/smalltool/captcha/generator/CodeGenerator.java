package cn.smalltool.captcha.generator;

/**
 * 验证码生成器
 */
public interface CodeGenerator {

    /**
     * 生成器
     * @return 验证码
     */
    String generate();

    /**
     * 校验
     */
    boolean verify(String var1, String var2);
}
