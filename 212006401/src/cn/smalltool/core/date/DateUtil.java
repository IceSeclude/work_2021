package cn.smalltool.core.date;

import cn.smalltool.core.utils.CalendarUtil;

import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil extends CalendarUtil {

    public DateUtil() {
    }

    public static DateTime date() {
        return new DateTime();
    }

    public static int thisYear() {
        return year(date());
    }

    public static int year(Date date) {
        return DateTime.of(date).year();
    }

    /**
     * 润年判断
     * @param year 年份值
     * @return true-闰年 false-平年
     */
    public static boolean isLeapYear(int year) {
        return (new GregorianCalendar()).isLeapYear(year);
    }

    public static DateTime parse(CharSequence dateStr, String format) {
        return new DateTime(dateStr, format);
    }
}
