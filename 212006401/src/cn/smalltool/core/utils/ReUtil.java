package cn.smalltool.core.utils;

import java.util.regex.Pattern;

/**
 * 正则工具
 */
public class ReUtil {

    public static boolean isMatch(Pattern pattern, CharSequence content) {
        return content != null && pattern != null && pattern.matcher(content).matches();
    }
}
