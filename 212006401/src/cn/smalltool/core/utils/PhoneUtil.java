package cn.smalltool.core.utils;

import cn.smalltool.core.lang.PatternPool;
import cn.smalltool.core.lang.Validator;

import java.util.regex.Pattern;

public class PhoneUtil {

    private static Pattern TEL = Pattern.compile("0\\d{2,3}-[1-9]\\d{6,7}");

    public static boolean isMobile(CharSequence value) {
        return Validator.isMatchRegex(PatternPool.MOBILE, value);
    }

    public static boolean isTel(CharSequence value) {
        return Validator.isMatchRegex(TEL, value);
    }

    public static boolean isPhone(CharSequence value) {
        return isMobile(value) || isTel(value);
    }

    public static CharSequence hideBefore(CharSequence phone) {
        return StrUtil.hide(phone, 0, 7);
    }

    public static CharSequence hideBetween(CharSequence phone) {
        return StrUtil.hide(phone, 3, 7);
    }

    public static CharSequence hideAfter(CharSequence phone) {
        return StrUtil.hide(phone, 7, 11);
    }

    public static CharSequence subBefore(CharSequence phone) {
        return StrUtil.sub(phone, 0, 3);
    }

    public static CharSequence subBetween(CharSequence phone) {
        return StrUtil.sub(phone, 3, 7);
    }

    public static CharSequence subAfter(CharSequence phone) {
        return StrUtil.sub(phone, 7, 11);
    }
}
