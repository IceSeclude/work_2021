package cn.smalltool.core.lang;

import cn.smalltool.core.utils.StrUtil;

public class Assert {

    public static <T extends CharSequence> T notBlank(T text) throws IllegalArgumentException {
        return notBlank(text, "[Assertion failed] - this String argument must have text; it must not be null, empty, or blank");
    }

    public static <T extends CharSequence> T notBlank(T text, String errorMsgTemplate, Object... params) throws IllegalArgumentException {
        if (StrUtil.isBlank(text)) {
            throw new IllegalArgumentException(StrUtil.format(errorMsgTemplate, params));
        } else {
            return text;
        }
    }
}
