package cn.smalltool.core.lang;

import java.util.regex.Pattern;

/**
 * 正则表达式库
 */
public class PatternPool {

    /**
     * 纯数字
     */
    public static final Pattern NUMBERS = Pattern.compile("\\d+");
    /**
     * 生日
     */
    public static final Pattern BIRTHDAY = Pattern.compile("^(\\d{2,4})([/\\-.年]?)(\\d{1,2})([/\\-.月]?)(\\d{1,2})日?$");
    /**
     * 中国手机号码
     */
    public static final Pattern MOBILE = Pattern.compile("(?:0|86|\\+86)?1[3-9]\\d{9}");

}
