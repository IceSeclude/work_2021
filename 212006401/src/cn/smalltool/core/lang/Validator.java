package cn.smalltool.core.lang;

import cn.smalltool.core.date.DateUtil;
import cn.smalltool.core.utils.ReUtil;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    public static final Pattern BIRTHDAY;

    static {
        BIRTHDAY = PatternPool.BIRTHDAY;
    }

    public static boolean isBirthday(CharSequence value) {
        Matcher matcher = BIRTHDAY.matcher(value);
        if (matcher.find()) {
            int year = Integer.parseInt(matcher.group(1));
            int month = Integer.parseInt(matcher.group(3));
            int day = Integer.parseInt(matcher.group(5));
            return isBirthday(year, month, day);
        } else {
            return false;
        }
    }

    //判断是否是有效日期
    public static boolean isBirthday(int year, int month, int day) {
        int thisYear = DateUtil.thisYear();
        if (year >= 1900 && year <= thisYear) {
            if (month >= 1 && month <= 12) {
                if (day >= 1 && day <= 31) {
                    if (day == 31 && (month == 4 || month == 6 || month == 9 || month == 11)) {
                        return false;
                    } else if (month != 2) {
                        return true;
                    } else {
                        return day < 29 || day == 29 && DateUtil.isLeapYear(year);
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static boolean isMatchRegex(Pattern pattern, CharSequence value) {
        return ReUtil.isMatch(pattern, value);
    }
}
