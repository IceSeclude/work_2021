package cn.smalltool.core.exception;

import cn.smalltool.core.utils.StrUtil;

public class ExceptionUtil {
    public ExceptionUtil() {
    }

    public static String getMessage(Throwable e) {
        return null == e ? "null" : StrUtil.format("{}: {}", new Object[]{e.getClass().getSimpleName(), e.getMessage()});
    }
}
